const express = require('express');
const biodataRouter = express.Router();

const { user_game, user_game_history, user_game_biodata } = require('../models')

biodataRouter.get('/biodata', (req, res) => {
    res.render('biodata')
});




module.exports = biodataRouter