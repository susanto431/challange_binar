const express = require('express');
const dashboardRouter = express.Router();

const { user_game, user_game_history, user_game_biodata } = require('../models')

dashboardRouter.get('/dashboard', (req, res) => {
    user_game.findAll({
        order: [
            ['id', 'ASC']
        ]
    }
    ).then(users => {
        return res.status(200).render('dashboard', { users })
    })
});

dashboardRouter.get('/delete/user/:id', (req, res) => {
    // console.log(id)
    user_game.destroy({
        where: { id: req.params.id }
    }).then((user_game) => {
        res.render('deleteID', { user_game })
    })
});

dashboardRouter.get('/edit/user/:id', (req, res) => {

    user_game.findOne({
        where: { id: req.params.id }
    }).then(biodata => {
        res.render('ShowBiodata', { biodata })
    })

});

dashboardRouter.post('/update/user/:id', (req, res) => {
    const { username, password } = req.body
    if (username === undefined || password === undefined) {
        return res.status(400).json("Data input kurang lengkap")
    }

    user_game.update({
        username: username,
        password: password
    }, {
        where: { id: req.params.id }
    }).then(user_game => {
        res.render("linkdashboard", { user_game })
    }).catch(err => {
        res.status(500).json("Masalah server")
    })
});

module.exports = dashboardRouter