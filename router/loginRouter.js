const express = require('express');
const loginRouter = express.Router();

const { user_game, user_game_history, user_game_biodata } = require('../models')



// Login

loginRouter.post('/login', (req, res) => {
    const { username, password } = req.body
    if (username === undefined || password === undefined) {
        return res.status(400).json("Data input kurang lengkap")
    }

    user_game.create({
        username: username,
        password: password
    }).then(user_game => {
        res.render("linkdashboard", { user_game })
    }).catch(err => {
        res.status(500).json('Masalah server')
    })

});

loginRouter.get('/login', (req, res) => {
    res.render('login')
});


module.exports = loginRouter


// Login (signup) => dashboard => tambah, detail (aksi = edit, delete)

// tambah => biodataRouter
// detail => forEach utk dashboard (dashboardRouter)