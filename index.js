const express = require('express');
const app = express();

const homeRouter = require('./router/homeRouter')
const gameRouter = require('./router/gameRouter')
const loginRouter = require('./router/loginRouter')
const biodataRouter = require('./router/biodataRouter')
const dashboardRouter = require('./router/dashboardRouter')

app.use(express.json());

app.use(express.urlencoded({
    extended: false
})
)


app.set('view engine', 'ejs');
app.use(homeRouter);
app.use(gameRouter);
app.use(loginRouter);
app.use(biodataRouter);
app.use(dashboardRouter);

// const user_game = require('./router/routeUser')
// app.use(user_game);






// static files
app.use(express.static('assets'))
app.use('/css', express.static(__dirname + '/assets/css'))
app.use('/js', express.static(__dirname + '/assets/js'))
app.use('/img', express.static(__dirname + '/assets/img'))






app.listen(3000, () => {
    console.log(`Server started on 3000`);
});